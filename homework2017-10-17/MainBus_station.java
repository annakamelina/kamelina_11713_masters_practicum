import java.util.*;
import java.io.*;

public class MainBus_station{
    public static void main(String[] argv) throws IOException{
        new MainBus_station().run();
    }

    PrintWriter pw;
    Scanner sc;
    public void run() throws IOException{
        sc = new Scanner(new File("input.txt"));

        pw = new PrintWriter(new File("output.txt"));
        int N;
        N=sc.nextInt();
        int []k=new int [N];
        int kol=1;
        for (int i=0;i<N;i++)
        {
            k[i]=sc.nextInt();
        }
        for (int i=0;i<N;i++)
        {
            if (k[i]>437)
            {
                kol=kol+1;
            }
            else
            {
                break;
            }
        }
        if(kol==N+1)
            pw.print("No crash");
        else
            pw.print("Crash "+kol);
        pw.close();
    }
}
