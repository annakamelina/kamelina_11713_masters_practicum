import java.util.*;
import java.io.*;

public class MainNumber_E{
    public static void main(String[] argv) throws IOException{
        new MainNumber_E().run();
    }

    PrintWriter pw;
    Scanner sc;
    public void run() throws IOException{
        sc = new Scanner(new File("input.txt"));
        pw = new PrintWriter(new File("output.txt"));
//2.7182818284590452353602875.
        int N;
        N=sc.nextInt();
        if (N==0)
            pw.print(3);
        if (N==1)
            pw.print(2.7);
        if (N==2)
            pw.print(2.72);
        if (N==3)
            pw.print(2.718);
        if (N==4)
            pw.print(2.7183);
        if (N==5)
            pw.print(2.71828);
        if (N==6)
            pw.print(2.718282);
        if (N==7)
            pw.print(2.7182818);
        if (N==8)
            pw.print(2.71828183);
        if (N==9)
            pw.print(2.718281828);
        if (N==10)
            pw.print(2.7182818285);
        if (N==11)
            pw.print(2.71828182846);
        if (N==12)
            pw.print(2.718281828459);
        if (N==13)
            pw.print(2.7182818284590);
        if (N==14)
            pw.print(2.71828182845905);
        if (N==15)
            pw.print(2.718281828459045);
        if (N==16)
            pw.print(2.7182818284590452);
        if (N==17)
            pw.print(2.71828182845904524);
        if (N==18)
            pw.print(2.718281828459045235);
        if (N==19)
            pw.print(2.7182818284590452354);
        if (N==20)
            pw.print(2.71828182845904523536);
        if (N==21)
            pw.print(2.718281828459045235360);
        if (N==22)
            pw.print(2.7182818284590452353603);
        if (N==23)
            pw.print(2.71828182845904523536029);
        if (N==24)
            pw.print(2.718281828459045235360288);
        if (N==25)
            pw.print(2.7182818284590452353602875);
        pw.close();
    }
}
