import java.util.*;
import java.io.*;

public class MainFlowers{
    public static void main(String[] argv) throws IOException{
        new MainFlowers().run();
    }
    PrintWriter pw;
    Scanner sc;
    public void run() throws IOException{
        sc = new Scanner(new File("input.txt"));
        int N=sc.nextInt();

        pw = new PrintWriter(new File("output.txt"));
        if (N % 3==0) pw.print("GCV");
        if(N % 3==1) pw.print("VGC");
        if(N % 3==2) pw.print("CVG");
        pw.close();
    }
}