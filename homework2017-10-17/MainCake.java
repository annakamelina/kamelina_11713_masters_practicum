import java.util.*;
import java.io.*;

public class MainCake{
    public static void main(String[] argv) throws IOException{
        new MainCake().run();
    }
    PrintWriter pw;
    Scanner sc;
    public void run() throws IOException{
        sc = new Scanner(new File("input.txt"));
        int N=sc.nextInt();
        int res=0;
        pw = new PrintWriter(new File("output.txt"));
        if (N > 1)
        {
            if (N % 2 == 0)
                res= N / 2;
            else
            {
                res= N;
            }
        }
        else
        {
            res= 0;
        }
        pw.print(res);
        pw.close();
    }
}