import java.util.*;
import java.io.*;

public class MainTurn{
    public static void main(String[] argv) throws IOException{
        new MainTurn().run();
    }

    PrintWriter pw;
    Scanner sc;
    public void run() throws IOException{
        sc = new Scanner(new File("input.txt"));

        pw = new PrintWriter(new File("output.txt"));
        int N;
        N=sc.nextInt();
        int [] a=new int [N];
        for (int i=0;i<N;i++)
            a[i]=sc.nextInt();
        for (int i=N-1;i>-1;i--)
            pw.print(a[i]+" ");


        pw.close();
    }
}
