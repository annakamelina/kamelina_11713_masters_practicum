import java.util.*;
import java.io.*;

public class MainSalary{
    public static void main(String[] argv) throws IOException{
        new MainSalary().run();
    }
    PrintWriter pw;
    Scanner sc;
    public void run() throws IOException{
        sc = new Scanner(new File("input.txt"));
        int a,b,c;
        a=sc.nextInt();
        b=sc.nextInt();
        c=sc.nextInt();
        int max = a;
        if (b > max)
            max = b;
        if (c > max)
            max = c;
        int min = a;
        if (b < min)
            min = b;
        if (c < min)
            min = c;
        int res=max-min;
        pw = new PrintWriter(new File("output.txt"));
        pw.print(res);
        pw.close();
    }
}