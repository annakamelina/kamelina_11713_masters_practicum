import java.util.*;
import java.io.*;

public class MainTwo_bandits{
    public static void main(String[] argv) throws IOException{
        new MainTwo_bandits().run();
    }

    PrintWriter pw;
    Scanner sc;
    public void run() throws IOException{
        sc = new Scanner(new File("input.txt"));

        pw = new PrintWriter(new File("output.txt"));
        int N,K;
        N=sc.nextInt();
        K=sc.nextInt();
        pw.println(K-1);
        pw.println(" ");
        pw.println(N-1);

        pw.close();
    }
}