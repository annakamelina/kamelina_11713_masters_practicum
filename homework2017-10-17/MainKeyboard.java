import java.util.*;
import java.io.*;

public class MainKeyboard{
    public static void main(String[] argv) throws IOException{
        new MainKeyboard().run();
    }
    PrintWriter pw;
    Scanner sc;
    public void run() throws IOException{
        sc = new Scanner(new File("input.txt"));
        char ans = 'f';
        char smb=sc.next().charAt(0);
        String s = "qwertyuiopasdfghjklzxcvbnm";
        for (int i=0;i<26;i++)
            if (smb == s.charAt(i))
            {
                if (i + 1 < 26)
                    ans = s.charAt(i+1);
                else
                {
                    ans = s.charAt(0);
                }
            }
        pw = new PrintWriter(new File("output.txt"));
        pw.print(ans);
        pw.close();
    }
}
