import java.util.*;
import java.io.*;

public class MainLazy_person{
    public static void main(String[] argv) throws IOException{
        new MainLazy_person().run();
    }

    PrintWriter pw;
    Scanner sc;
    public void run() throws IOException{
        sc = new Scanner(new File("input.txt"));

        pw = new PrintWriter(new File("output.txt"));
        int N;
        N=sc.nextInt();
        int [][] a=new int [N][2];
        for (int i=0;i<N;i++)
            for (int j=0;j<2;j++)
                a[i][j]=sc.nextInt();
        int upbound=a[0][0];
        int downbound=a[0][1];
        for (int i=1;i<N;i++)
        {
            if (a[i][0]>upbound)
                upbound=a[i][0];
            if (a[i][1]<downbound)
                downbound=a[i][1];
        }
        if (upbound<=downbound)
            pw.print("YES");
        else
            pw.print("NO");
        pw.close();
    }
}
