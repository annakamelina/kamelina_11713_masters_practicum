import java.util.*;
import java.io.*;

public class Main_Enium{
    public static void main(String[] argv) throws IOException{
        new Main_Enium().run();
    }

    PrintWriter pw;
    Scanner sc;
    public void run() throws IOException{
        sc = new Scanner(new File("input.txt"));

        pw = new PrintWriter(new File("output.txt"));
        int N,K,M;
        N=sc.nextInt();
        K=sc.nextInt();
        M=sc.nextInt();
        pw.print(K*M*2*N);

        pw.close();
    }
}
